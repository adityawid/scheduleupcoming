package com.devjurnal.katalogmovie.AlarmNotif;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.devjurnal.katalogmovie.BuildConfig;
import com.devjurnal.katalogmovie.MainActivity;
import com.devjurnal.katalogmovie.R;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by DevJurnal on 1/26/18.
 */
// TODO ALARM 
public class SchedulerService extends GcmTaskService {
    public static String SCHEDULER_UPCOMING = "upcoming";
    private String linkurl="";
    private String imageURL = BuildConfig.IMAGEURL;

    @Override
    public int onRunTask(TaskParams taskParams) {
        int result = 0;
        if (taskParams.getTag().equals(SCHEDULER_UPCOMING)) {
            getDataOnline();
            result = GcmNetworkManager.RESULT_SUCCESS;
        }

        return result;
    }
    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        SchedulerTask mSchedulerTask = new SchedulerTask(this);
        mSchedulerTask.createPeriodicTask();
    }
    private void showNotification(Context context, String title, String message, int notifId){
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent;
        intent = new Intent(context,MainActivity.class);
        PendingIntent pendingIntent =  PendingIntent.getActivity(context, notifId, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setColor(ContextCompat.getColor(context, android.R.color.black))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);
        notificationManagerCompat.notify(notifId, builder.build());
    }

    private void getDataOnline() {
        linkurl = BuildConfig.UPCOMING_URL;
        JsonObjectRequest ambildata = new JsonObjectRequest(Request.Method.GET, linkurl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrayresults = response.getJSONArray("results");

                    if (arrayresults.length() == 0 ){
                        Log.d("upcoming" , "tidak ada data");
                    }else {
                        JSONObject json = arrayresults.getJSONObject(0);
                        String title = json.getString("title");
                        String message = title+ " telah rilis";
                        int notifId = 200;

                        Log.d("movie service:", title);

                        showNotification(getApplicationContext(), title, message, notifId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(ambildata);
    }

}
