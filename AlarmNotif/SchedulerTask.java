package com.devjurnal.katalogmovie.AlarmNotif;

import android.content.Context;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

/**
 * Created by DevJurnal on 1/26/18.
 */
// TODO ALARM
public class SchedulerTask {
    private GcmNetworkManager mGcmNetworkManager;
    public SchedulerTask(Context context){
        mGcmNetworkManager = GcmNetworkManager.getInstance(context);
    }
    public void createPeriodicTask() {
        Task periodicTask = new PeriodicTask.Builder()
                .setService(SchedulerService.class)
                .setPeriod(60*1000)
                .setFlex(10)
                .setTag(SchedulerService.SCHEDULER_UPCOMING)
                .setPersisted(true)
                .build();
        mGcmNetworkManager.schedule(periodicTask);
    }
    public void cancelPeriodicTask(){
        if (mGcmNetworkManager != null){
            mGcmNetworkManager.cancelTask(SchedulerService.SCHEDULER_UPCOMING, SchedulerService.class);
        }
    }
}
